<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', 'Product\ProductsController@index')->name('products');
 
Route::get('cart', 'Product\ProductsController@cart')->name('cart');
 
Route::get('add-to-cart/{id}', 'Product\ProductsController@addToCart')->name('addtocart');

Route::patch('update-cart', 'Product\ProductsController@update')->name('updatecart');
 
Route::delete('remove-from-cart', 'Product\ProductsController@remove')->name('removeFromCart');