<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductsController extends Controller
{
	/*
	* This function is used to show product listing
	*/
    public function index()
    {
    	$products = Product::all();
 
        return view('products.products',compact('products'));
    }
 
    public function cart()
    {
        return view('carts.carts');
    }

    public function addToCart($id)
    {
 		$product = Product::find($id);
 
        if(!$product) {
            abort(404);
        }
 
        $cart = session()->get('cart');
 
        // if cart is empty then this the first product
        if(!$cart) {
 
            $cart = [
                    $id => [
                        "name" => $product->name,
                        "quantity" => 1,
                        "price" => $product->price,
                        "photo" => $product->image
                    ]
            ];

        	$offredProducts = $product->offeredProducts;
           	foreach ($offredProducts as $key => $value) {
           		$cart[$id]['offeredItems'][] = [
                        "name" => $value->name,
                        "quantity" => 1,
                        "price" => $value->price,
                        "photo" => $value->image
                    ];
           	}
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
 		
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
 
            $cart[$id]['quantity']++;

            if(isset($cart[$id]['offeredItems']))
            {
				foreach ($cart[$id]['offeredItems'] as $key => $value) {
           			$cart[$id]['offeredItems'][$key]['quantity']++;
           		}
            }
 
            session()->put('cart', $cart);
 
            return redirect()->back()->with('success', 'Product added to cart successfully!');
 
        }
 
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $product->name,
            "quantity" => 1,
            "price" => $product->price,
            "photo" => $product->image
        ];

        $offredProducts = $product->offeredProducts;
           	foreach ($offredProducts as $key => $value) {
           		$cart[$id]['offeredItems'][] = [
                        "name" => $value->name,
                        "quantity" => 1,
                        "price" => $value->price,
                        "photo" => $value->image
                    ];
           	}
 
        session()->put('cart', $cart);
 
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

	public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
 
            $cart[$request->id]["quantity"] = $request->quantity;

        	if(isset($cart[$request->id]['offeredItems']))
            {
				foreach ($cart[$request->id]['offeredItems'] as $key => $value) {
           			$cart[$request->id]['offeredItems'][$key]['quantity']++;
           		}
            }
 
            session()->put('cart', $cart);
 
            session()->flash('success', 'Cart updated successfully');
        }
    }
 
    public function remove(Request $request)
    {
        if($request->id) {
 
            $cart = session()->get('cart');
 
            if(isset($cart[$request->id])) {
 
                unset($cart[$request->id]);
 
                session()->put('cart', $cart);
            }
 
            session()->flash('success', 'Product removed successfully');
        }
    }
}
