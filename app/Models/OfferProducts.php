<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferProducts extends Model
{
    protected $table='offer_product_items';
}
