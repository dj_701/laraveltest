<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';

    public function offeredProducts()
    {
    	return $this->belongsToMany('App\Models\Product','offer_product_items','product_id','offered_item_id');
    }
}
